package com.springboot.empresa.jdbc.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.springboot.empresa.jdbc.model.Recibo;

public class ReciboRowMapper implements RowMapper<Recibo>{
	@Override
	public Recibo mapRow(ResultSet rs, int rowNum) throws SQLException {
		Recibo recibo = new Recibo();
		
		recibo.setRno_recibo(rs.getString("nroRecibo"));
		recibo.setMonto_emitido(rs.getDouble("montoEmitido"));
		recibo.setFg_retencion(rs.getString("fgRetencion"));
		
		return recibo;
	}
}
