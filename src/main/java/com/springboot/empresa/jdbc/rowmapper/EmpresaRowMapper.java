package com.springboot.empresa.jdbc.rowmapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.springboot.empresa.jdbc.model.Empresa;

public class EmpresaRowMapper implements RowMapper<Empresa>{
	@Override
	public Empresa mapRow(ResultSet rs, int rowNum) throws SQLException {
		Empresa persona = new Empresa();
		
		persona.setId_empresa(rs.getInt("idEmpresa"));
		persona.setRuc(rs.getString("ruc"));
		persona.setRazon_social(rs.getString("razonSocial"));
		persona.setEstado_actual(rs.getString("estadoActual"));
		
		return persona;
	}
}
