package com.springboot.empresa.jdbc.dao.impl;

import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import com.springboot.empresa.jdbc.dao.EmpresaDao;
import com.springboot.empresa.jdbc.model.Empresa;
import com.springboot.empresa.jdbc.model.Recibo;
import com.springboot.empresa.jdbc.rowmapper.EmpresaRowMapper;
import com.springboot.empresa.jdbc.rowmapper.ReciboRowMapper;

@Repository
public class EmpresaDaoImpl extends JdbcDaoSupport implements EmpresaDao {

	public EmpresaDaoImpl(DataSource dataSource) {
		this.setDataSource(dataSource);
	}

	@Override
	public void saveEmpresa(Empresa empresa) {
		String sql = "insert into microservicios.empresas (ruc, razonSocial, estadoActual) " + "values (?, ?, ?);";

		Object[] params = { empresa.getRuc(), empresa.getRazon_social(), empresa.getEstado_actual() };
		int[] tipos = { Types.VARCHAR, Types.VARCHAR, Types.VARCHAR };

		try {

			int filas = getJdbcTemplate().update(sql, params, tipos);

			logger.debug("Se han insertado : " + filas + " filas");
			logger.debug("Se ha registrado a la empresa " + empresa.toString());

		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}

	@Override
	public List<Empresa> getAllEmpresas() {
		List<Empresa> listaEmpresas = new ArrayList<Empresa>();

		String sql = " SELECT idEmpresa, ruc, razonSocial, estadoActual FROM microservicios.empresas";

		try {

			RowMapper<Empresa> empresaRow = new EmpresaRowMapper();
			listaEmpresas = getJdbcTemplate().query(sql, empresaRow);
			logger.debug("Se han listado " + listaEmpresas.size() + " empresas");

		} catch (Exception e) {
			logger.error(e.getMessage());
		}

		return listaEmpresas;
	}

	@Override
	public Empresa getEmpresa(Integer id_empresa) {
		Empresa empresa = new Empresa();
		List<Empresa> listaEmpresas = new ArrayList<Empresa>();
		List<Recibo> listaRecibos = new ArrayList<Recibo>();
		
		String sql = " SELECT idEmpresa, ruc, razonSocial, estadoActual\n" + 
				" FROM microservicios.empresas where idEmpresa='"+id_empresa+"'";
				
		try {
			
			RowMapper<Empresa> empresaRow = new EmpresaRowMapper();
			listaEmpresas = getJdbcTemplate().query(sql, empresaRow);
			
			empresa = listaEmpresas.get(0);
			
			logger.debug("Se ha traido a la empresa "+listaEmpresas.get(0).toString());
			
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		
		sql = " SELECT nroRecibo, montoEmitido, fgRetencion\n" + 
				" FROM microservicios.recibo where idEmpresa='"+id_empresa+"'";
				
		try {
			
			RowMapper<Recibo> reciboRow = new ReciboRowMapper();
			listaRecibos = getJdbcTemplate().query(sql, reciboRow);
			
			empresa.setRecibo(listaRecibos);
			
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		
		return empresa;
	}

	@Override
	public void saveRecibo(Recibo recibo) {
		String sql = "insert into microservicios.recibo (idEmpresa, nroRecibo, montoEmitido, fgRetencion) "
				+ "values (?, ?, ?);";

		Object[] params = { recibo.getId_empresa(), recibo.getRno_recibo(), recibo.getMonto_emitido(),
				recibo.getFg_retencion() };
		int[] tipos = { Types.INTEGER, Types.VARCHAR, Types.NUMERIC, Types.VARCHAR };

		try {

			int filas = getJdbcTemplate().update(sql, params, tipos);

			logger.debug("Se han insertado : " + filas + " filas");
			logger.debug("Se ha registrado al recibo " + recibo.toString());

		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}
}
