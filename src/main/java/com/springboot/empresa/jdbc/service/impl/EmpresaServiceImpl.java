package com.springboot.empresa.jdbc.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springboot.empresa.jdbc.dao.impl.EmpresaDaoImpl;
import com.springboot.empresa.jdbc.model.Empresa;
import com.springboot.empresa.jdbc.model.Recibo;
import com.springboot.empresa.jdbc.service.EmpresaService;

@Service
public class EmpresaServiceImpl implements EmpresaService {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private EmpresaDaoImpl _empresaDao;

	public void saveEmpresa(Empresa empresa) {
		try {
			_empresaDao.saveEmpresa(empresa);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}

	public List<Empresa> getAllEmpresas() {
		return _empresaDao.getAllEmpresas();
	}

	public Empresa getEmpresa(Integer id_empresa) {
		return _empresaDao.getEmpresa(id_empresa);
	}

	public void saveRecibo(Recibo recibo) {
		try {
			_empresaDao.saveRecibo(recibo);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}

}
