package com.springboot.empresa.jdbc.service;

import java.util.List;

import com.springboot.empresa.jdbc.model.Empresa;
import com.springboot.empresa.jdbc.model.Recibo;

public interface EmpresaService {
	
	void saveEmpresa(Empresa empresa);
	List<Empresa> getAllEmpresas();
	Empresa getEmpresa(Integer id_empresa);
	void saveRecibo(Recibo recibo);
}
