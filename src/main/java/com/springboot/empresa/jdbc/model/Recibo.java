package com.springboot.empresa.jdbc.model;

public class Recibo {
	
	private Integer id_recibo;
	private Integer id_empresa;
	private String rno_recibo;
	private Double monto_emitido;
	private String fg_retencion;
	
	public Recibo() {

	}

	public Recibo(Integer id_recibo, Integer id_empresa, String rno_recibo, Double monto_emitido, String fg_retencion) {

		this.id_recibo = id_recibo;
		this.id_empresa = id_empresa;
		this.rno_recibo = rno_recibo;
		this.monto_emitido = monto_emitido;
		this.fg_retencion = fg_retencion;
	}

	public Integer getId_recibo() {
		return id_recibo;
	}

	public void setId_recibo(Integer id_recibo) {
		this.id_recibo = id_recibo;
	}

	public Integer getId_empresa() {
		return id_empresa;
	}

	public void setId_empresa(Integer id_empresa) {
		this.id_empresa = id_empresa;
	}

	public String getRno_recibo() {
		return rno_recibo;
	}

	public void setRno_recibo(String rno_recibo) {
		this.rno_recibo = rno_recibo;
	}

	public Double getMonto_emitido() {
		return monto_emitido;
	}

	public void setMonto_emitido(Double monto_emitido) {
		this.monto_emitido = monto_emitido;
	}

	public String getFg_retencion() {
		return fg_retencion;
	}

	public void setFg_retencion(String fg_retencion) {
		this.fg_retencion = fg_retencion;
	}
	
	@Override
	public String toString() {
		return "Empresa [id_recibo=" + id_recibo + ", id_empresa=" + id_empresa + ", rno_recibo=" + rno_recibo
				+ ", monto_emitido=" + monto_emitido + ", fg_retencion=" + fg_retencion  + "]";
	}
}
