package com.springboot.empresa.jdbc.model;

public class ResponseBean {
	
	private String codigo_servicio;
	private String descripcion;
	
	public String getCodigo_servicio() {
		return codigo_servicio;
	}
	public void setCodigo_servicio(String codigo_servicio) {
		this.codigo_servicio = codigo_servicio;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
}
