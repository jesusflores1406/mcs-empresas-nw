package com.springboot.empresa.jdbc.model;

import java.util.List;

public class Empresa {

	private Integer id_empresa;
	private String ruc;
	private String razon_social;
	private String estado_actual;
	private List<Recibo> recibo;

	public Empresa() {

	}

	public Empresa(Integer id_empresa, String ruc, String razon_social, String estado_actual) {

		this.id_empresa = id_empresa;
		this.ruc = ruc;
		this.razon_social = razon_social;
		this.estado_actual = estado_actual;
	}

	public Integer getId_empresa() {
		return id_empresa;
	}

	public void setId_empresa(Integer id_empresa) {
		this.id_empresa = id_empresa;
	}

	public String getRuc() {
		return ruc;
	}

	public void setRuc(String ruc) {
		this.ruc = ruc;
	}

	public String getRazon_social() {
		return razon_social;
	}

	public void setRazon_social(String razon_social) {
		this.razon_social = razon_social;
	}

	public String getEstado_actual() {
		return estado_actual;
	}

	public void setEstado_actual(String estado_actual) {
		this.estado_actual = estado_actual;
	}
	
	public List<Recibo> getRecibo() {
		return recibo;
	}

	public void setRecibo(List<Recibo> recibo) {
		this.recibo = recibo;
	}
	
	@Override
	public String toString() {
		return "Empresa [id_empresa=" + id_empresa + ", ruc=" + ruc + ", razon_social=" + razon_social
				+ ", estado_actual=" + estado_actual + "]";
	}
	
}
