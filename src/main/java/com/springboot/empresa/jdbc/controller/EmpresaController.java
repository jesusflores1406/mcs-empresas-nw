package com.springboot.empresa.jdbc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.empresa.jdbc.model.Empresa;
import com.springboot.empresa.jdbc.model.ResponseBean;
import com.springboot.empresa.jdbc.service.impl.EmpresaServiceImpl;

@RestController
@RequestMapping("/empresa")
public class EmpresaController {
	
	@Autowired
	private EmpresaServiceImpl _empresaService;
	
	@PostMapping(value = "/", produces = "application/json")	
	public ResponseBean saveEmpresa(@RequestBody Empresa empresa){
		
		ResponseBean resp = new ResponseBean();
		
		_empresaService.saveEmpresa(empresa);
		
		resp.setCodigo_servicio("0000");
		resp.setDescripcion("Empresa registrada con éxito");
		
		return resp;
	}
	
	@GetMapping(value = "/", produces = "application/json")	
	public List<Empresa> getAllEmpresa(){
		return _empresaService.getAllEmpresas();
	}
	
	@GetMapping(value = "/{id}", produces = "application/json")	
	public Empresa getEmpresa(@PathVariable ("id_empresa") Integer id_empresa){
		return _empresaService.getEmpresa(id_empresa);
	}
}
