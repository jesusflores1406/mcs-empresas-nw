package com.springboot.empresa.jdbc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.empresa.jdbc.model.Recibo;
import com.springboot.empresa.jdbc.model.ResponseBean;
import com.springboot.empresa.jdbc.service.impl.EmpresaServiceImpl;

@RestController
@RequestMapping("/recibo")
public class ReciboController {

	@Autowired
	private EmpresaServiceImpl _empresaService;

	@PostMapping(value = "/", produces = "application/json")
	public ResponseBean saveEmpresa(@RequestBody Recibo recibo) {

		ResponseBean resp = new ResponseBean();

		_empresaService.saveRecibo(recibo);
		
		resp.setCodigo_servicio("0000");
		resp.setDescripcion("Recibo registrado con éxito");

		return resp;
	}
}
